<div class="page-header">
  <h3>Ubah Kamar</h3>
</div>
<?php
$ambil=$koneksi->query("SELECT * FROM kamar WHERE id_kamar='$_GET[id]'");
$pecah=$ambil->fetch_assoc();
?>
<form method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="tipekamar">Tipe Kamar</label>
    <select name="tipekamar" id="tipekamar"class="form-control" required>
      <option value="">-Pilih Tipe Kamar-</option>
      <?php 
      $sql_kamar = $koneksi->query($con. "SELECT * FROM tipekamar") or die (mysqli_eror($con));
      while ($data_kamar = mysqli_fetch_array($sql_kamar)) {
        echo '<option value="'.$data_kamar['id_tipe'].'">'.$data_kamar['tipe_kamar'].'</option>';  
      } ?>
       </select>
  </div>
   <div class="form-group">
    <label>Nama Kamar</label>
    <input type="text"  name="nama" class="form-control" value="<?php echo $pecah['nama_kamar'] ?>">
  </div>
 <div class="form-group">
    <label>Nomor Kamar</label>
    <input type="text" name="no" class="form-control" value="<?php echo $pecah['no_kamar'] ?>">
  </div>
  <div class="form-group">
    <label>Tipe Kasur</label>
    <select name="tipe_kasur" class="form-control">
      <option <?php if($pecah['tipe_kasur'] == "Single Bed"){echo "selected='selected'";} echo $pecah['tipe_kasur']; ?> value="Single Bed">Single Bed</option>
      <option <?php if($pecah['tipe_kasur'] == "Twin Bed"){echo "selected='selected'";} echo $pecah['tipe_kasur']; ?> value="Twin Bed">Twin Bed</option>
      <option <?php if($pecah['tipe_kasur'] == "Double Bed"){echo "selected='selected'";} echo $pecah['tipe_kasur']; ?> value="Double Bed">Double Bed</option>
    </select>
  </div>
  <div class="form-group">
    <label>Harga Kamar</label>
    <input type="text" name="harga" class="form-control" value="<?php echo $pecah['harga_kamar'] ?>">
  </div>

  <div class="form-group">
    <label>Lokasi</label>
    <select name="lokasi" class="form-control">
      <option <?php if($pecah['lokasi'] == "Lantai 1"){echo "selected='selected'";} echo $pecah['lokasi']; ?> value="Lantai 1">Lantai 1</option>
      <option <?php if($pecah['lokasi'] == "Lantai 2"){echo "selected='selected'";} echo $pecah['lokasi']; ?> value="Lantai 2">Lantai 2</option>
      <option <?php if($pecah['lokasi'] == "Lantai 3"){echo "selected='selected'";} echo $pecah['lokasi']; ?> value="Lantai 3">Lantai 3</option>
    </select>
  </div>

  <div class="form-group">
    <img src="../foto_produk/<?php echo $pecah['gambar'] ?>" width="200">
  </div>
  <dir class="form-group">
    <label>Foto Kamar</label>
<!--    <?php
    // if(isset($pecah['gambar'])){
      //  echo '<input type="hidden" name="old_pict" value="'.$pecah['gambar'].'">';
       // echo '<img src="../foto_produk/" width="30%">';
      //}
    ?>
    <input type="hidden" name="old_pict" value="<?php echo $pecah['gambar'] ?>">-->
    <input name="fotokamar" type="file" class="form-control" >
  </dir>
  <div class="form-group">
    <input type="submit" value="Perbarui" name="ubah" class="btn btnprimary">
  </div>
</form>

<?php
if (isset($_POST['ubah'])) {
  $id_kamar = $_GET['id'];
  $tipekamar = $_POST['tipekamar']; 
  $nama_kamar = $_POST['nama'];
  $no_kamar = $_POST['no'];
  $tipe_kasur = $_POST['tipe_kasur'];
  $lokasi = $_POST['lokasi'];
  $harga_kamar = $_POST['harga'];

  $lama = $_POST['old_pict'];

  $nama = $_FILES['fotokamar']['name'];
  $hihi =$_FILES['fotokamar']['tmp_name'];
  $up = move_uploaded_file($hihi, "../foto_produk/".$nama);
  //unlink('../foto_produk'.$lama);

  if (!$up) {

    //$nama = $_FILES['old_pict']['name'];
    //$hihi =$_FILES['old_pict']['tmp_name'];
    //move_uploaded_file($hihi, "../foto_produk/".$nama);

    $koneksi->query("UPDATE kamar SET id_tipe = '$tipekamar', nama_kamar = '$nama_kamar', no_kamar = '$no_kamar', tipe_kasur = '$tipe_kasur', lokasi = '$lokasi', harga_kamar = '$harga_kamar' WHERE id_kamar = '$id_kamar'");  

    if ($koneksi) {
      echo "<script>alert('data kamar telah diubah');</script>";
      echo "<script>location='index.php?halaman=kamar';</script>";
    }
  }
  else{
    //$nama = $_FILES['fotokamar']['name'];
    //$hihi =$_FILES['fotokamar']['tmp_name'];
    //move_uploaded_file($hihi, "../foto_produk/".$nama);

    $koneksi->query("UPDATE kamar SET id_tipe = '$tipekamar', nama_kamar = '$nama_kamar', no_kamar = '$no_kamar', tipe_kasur = '$tipe_kasur', lokasi = '$lokasi', gambar = '$nama', harga_kamar = '$harga_kamar' WHERE id_kamar = '$id_kamar'"); 

    if ($koneksi) {
      echo "<script>alert('data kamar telah diubah');</script>";
      echo "<script>location='index.php?halaman=kamar';</script>";
    }
  }
}

/*if (isset($_POST['ubah'])) 
{
  $namafoto=$_FILES['fotokamar']['name'];
  $lokasifoto=$_FILES['fotokamar']['tmp_name'];
  //jika foto di rubah
  if (!empty($lokasifoto)) 
  {
    move_uploaded_file($lokasifoto, "../foto_produk/$namafoto");

    $koneksi->query("UPDATE kamar SET tipe_kamar='$_POST[tipekamar]' nama_kamar='$_POST[nama]', no_kamar='$_POST[no]',tipe_kasur='$_POST[tipe]',harga_kamar='$_POST[harga]',lokasi='$_POST[lokasi]',status_kamar='$_POST[status]',foto_produk='$namafoto' WHERE id_kamar='$_GET[id]'");
  }
  else
  {
    $koneksi->query("UPDATE kamar SET tipe_kamar='$_POST[tipekamar]' nama_kamar='$_POST[nama]', no_kamar='$_POST[no]',tipe_kasur='$_POST[tipe_kasur]',harga_kamar='$_POST[harga]',lokasi='$_POST[lokasi]',status_kamar='$_POST[status]'WHERE id_kamar='$_GET[id]'");
  }
  echo "<script>alert('data kamar telah diubah');</script>";
  echo "<script>location='index.php?halaman=kamar';</script>";
  
}*/
?>