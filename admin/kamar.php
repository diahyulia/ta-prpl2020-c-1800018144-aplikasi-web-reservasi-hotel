<div class="page-header">
  <h3>Data Kamar</h3>
</div>
<a href="index.php?halaman=tambahkamar" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Kamar Baru</a>
<br/><br/>
<table class="table table-bordered" >
	<thead>
		<tr>
			<th>No</th>
			<th>Nama kamar</th>
			<th>Foto kamar</th>
			<th>No kamar</th>
			<th>Tipe kasur</th>
			<th>Harga kamar</th>
			<th>Lokasi Kamar</th>
			<th>Status Kamar</th>
			<th>aksi</th>
		</tr>
	</thead>
	<body>
		<?php $nomor=1; ?>
		<?php $ambil=$koneksi->query("SELECT * FROM kamar"); ?>
		<?php while($pecah = $ambil->fetch_assoc()) { ?>
		<tr>
			<td><?php echo $nomor; ?></td>
			<td><?php echo $pecah['nama_kamar']; ?></td>
			<td>
				<img src="../foto_produk/<?php echo $pecah['gambar']; ?>" width="100"></td>
			<td><?php echo $pecah['no_kamar']; ?></td>
			<td><?php echo $pecah['tipe_kasur']; ?></td>
			<td><?php echo "Rp. ".number_format($pecah['harga_kamar']).",-" ?></td>
			<td><?php echo $pecah['lokasi']; ?></td>
			<td><?php echo $pecah['status_kamar']; ?></td>
			
			<td>
				<a href="index.php?halaman=hapuskamar&id=<?php echo $pecah['id_kamar']; ?>" class="btn-danger btn">hapus</a>

				<a href="index.php?halaman=ubahkamar&id=<?php echo $pecah['id_kamar']; ?>" class="btn btn-warning">ubah</a>

			</td>
		</tr>
		<?php $nomor++ ?>
		<?php } ?>
	</body>
</table>

