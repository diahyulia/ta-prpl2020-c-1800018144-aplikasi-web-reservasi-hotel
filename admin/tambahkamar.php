

<div class="page-header">
  <h3>Kamar Baru</h3>
</div>
<form method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="tipekamar">Tipe Kamar</label>
    <select name="tipekamar" id="tipekamar"class="form-control" required>
      <option value="">-Pilih Tipe Kamar-</option>
      <?php 
      $sql_kamar = $koneksi->query($con. "SELECT * FROM tipekamar") or die (mysqli_eror($con));
      while ($data_kamar = mysqli_fetch_array($sql_kamar)) {
        echo '<option value="'.$data_kamar['id_tipe'].'">'.$data_kamar['tipe_kamar'].'</option>';  
      } ?>
       </select>
  </div>
	<div class="form-group">
    <label>Nama Kamar</label>
    <input type="text" name="nama_kamar" class="form-control">
  </div>

  <div class="form-group">
    <label>Nomor Kamar</label>
    <input type="text" name="no_kamar" class="form-control">
  </div>

  <div class="form-group">
    <label>Tipe Kasur</label>
    <select name="tipe_kasur" class="form-control">
      <option value="">-Pilih Tipe Kasur-</option>
      <option value="Single Bed">Single Bed</option>
      <option value="Twin Bed">Twin Bed</option>
      <option value="Double Bed">Double Bed</option>
    </select>
  </div>
  <div class="form-group">
    <label>Harga Kamar</label>
    <input type="text" name="harga_kamar" class="form-control">
  </div>
 <div class="form-group">
    <label>Lokasi</label>
    <select name="lokasi" class="form-control">
      <option value="">-Pilih Lokasi Kamar-</option>
      <option value="Lantai 1">Lantai 1</option>
      <option value="Lantai 2">Lantai 2</option>
      <option value="Lantai 3">Lantai 3</option>
    </select>
  </div>
	<div class="form-group">
		<label>Foto Kamar</label>
		<input type="file" class="form-control" name="fotokamar">
	</div>
	<button class="btn btn-primary" name="save">Simpan</button>
</form>
<?php
if (isset($_POST['save'])) 
{
  $id_tipe = $_POST['tipekamar'];
  $nama_kamar = $_POST['nama_kamar'];
  $no_kamar = $_POST['no_kamar'];
  $tipe_kasur = $_POST['tipe_kasur'];
  $lokasi = $_POST['lokasi'];
  $harga_kamar = $_POST['harga_kamar'];
  $status_kamar = 'tersedia';

	$nama = $_FILES['fotokamar']['name'];
	$hihi =$_FILES['fotokamar']['tmp_name'];
	move_uploaded_file($hihi, "../foto_produk/".$nama);

	$koneksi->query("INSERT INTO kamar(id_tipe, nama_kamar, no_kamar, tipe_kasur, lokasi, harga_kamar, status_kamar, gambar) VALUES('$id_tipe','$nama_kamar','$no_kamar','$tipe_kasur','$lokasi', '$harga_kamar' ,'$status_kamar', '$nama')");

	echo "<div class='alert alert-info'>Data tersimpan</div>";
  echo "<meta http-equiv='refresh' conten='1;url=index.php?halaman=kamar'>";
}
?>

