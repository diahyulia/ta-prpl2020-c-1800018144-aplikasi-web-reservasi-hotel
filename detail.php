<?php 

include 'layout/header.php';

$id = $_GET['id'];

$kamar = mysqli_query($koneksi, "SELECT kamar.id_kamar, kamar.nama_kamar, kamar.harga_kamar, kamar.id_tipe, tipekamar.tipe_kamar, kamar.gambar, kamar.status_kamar, kamar.no_kamar FROM kamar join tipekamar on kamar.id_tipe = tipekamar.id_tipe where kamar.id_kamar = '$id'"); 
$tipe = mysqli_query($koneksi, "SELECT * FROM tipekamar");

?>

<div class="inside-banner">
  	<div class="container"> 
    	<h2>Info Kamar</h2>
	</div>
</div>

<div class="container">
	<div class="properties-listing spacer">
		<?php foreach ($kamar as $value):?>
		<h2><?php echo $value['no_kamar'];?></h2>
		<div class="row">
			<div class="col-sm-7">
				<img src="foto_produk/<?=$value['gambar'];?>" class="properties" alt="properties" style="width: 100%;">
				<div class="spacer"><h4><span class="glyphicon glyphicon-th-list"></span> Informasi Kamar</h4>
	  			<p>harusnya fasilitas disini</p> 
	  			</div>
			</div>
			<div class="col-sm-4" style="margin-left: 30px;">
						<div class="property-info">
							<p class="price"><?=$value['harga_kamar'];?></p>
						</div>

		    			<h6><span class="glyphicon glyphicon-home"></span><?php echo $value['nama_kamar'];?></h6>

						<?php if ($value['status_kamar']=='tersedia') { ?>

				  		<h6><span class="glyphicon glyphicon-envelope"></span> Pemesanan Kamar</h6>
						  
						  <form method="post" action="proses_sewa.php">
						  		<input type="hidden" name="harga" value="<?=$value['harga_kamar'];?>">
							  	<input type="hidden" name="id_kamar" value="<?=$value['id_kamar'];?>">
							    <div class="input-group col-md-12">
							       <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							           	<input class="form-control"  type="date" name="tgl_cekin" placeholder="Tanggal Chek In">
							   		 <br>
							    	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							        	<input class="form-control"  type="date" name="tgl_cekout" placeholder="Tanggal Chek Out">
								</div>
							    <br>
							    <input type="text" class="form-control" name="nama_pelanggan" placeholder="Nama"/>
							    <div style="margin-bottom: 17px;">
									<input type="radio" name="gender" value="Laki-Laki">Laki Laki
									<input type="radio" name="gender" value="Perempuan">Perempuan
								</div>
							    <input type="text" class="form-control" name="no_telp" placeholder="Tlp"/>
							    <input type="text" class="form-control" name="email" placeholder="email"/>
							    <textarea rows="4" class="form-control" name="alamat" placeholder="Alamat"></textarea>
							    <button type="submit" class="btn btn-primary" name="Submit" style="margin-top: 20px;">Booking Kamar</button>
						  </form>

				  		<?php } else { ?>

						<div class='alert alert-danger'>
						    <span>Kamar Not Avaliable</span>  
						</div>

						<?php } ?>
					
	  		</div>
 		</div>         
	</div>
</div>

<?php 

endforeach;

include 'layout/footer.php'; 

?>