<?php 

include 'layout/header.php';

$kamar = mysqli_query($koneksi, "SELECT kamar.id_kamar, kamar.nama_kamar, kamar.harga_kamar, kamar.id_tipe, tipekamar.tipe_kamar, kamar.gambar, kamar.status_kamar, kamar.no_kamar FROM kamar join tipekamar on kamar.id_tipe = tipekamar.id_tipe order by id_kamar DESC"); 
$tipe = mysqli_query($koneksi, "SELECT * FROM tipekamar");

if (isset($_POST['id_tipe'])) {
	$key = $_POST['id_tipe'];

	$cari = mysqli_query($koneksi, "SELECT kamar.id_kamar, kamar.nama_kamar, kamar.harga_kamar, kamar.id_tipe, tipekamar.tipe_kamar, kamar.gambar, kamar.status_kamar, kamar.no_kamar FROM kamar join tipekamar on kamar.id_tipe = tipekamar.id_tipe where kamar.id_tipe like '$key' order by id_kamar DESC "); 
}
else{
	$cari = $kamar;
}

?>

	<div class="inside-banner">
	  <div class="container"> 
	    
	    <h2>Kamar</h2>
	</div>
	</div>
	<!-- banner -->


	<div class="container">
	<div class="properties-listing spacer">

	<div class="row">
	<div class="col-lg-3 col-sm-4 ">

	  <div class="search-form"><h4><span class="glyphicon glyphicon-search"></span> Search for</h4>
	          <form method="post" action="">
	          <div class="row">
	          <div class="col-lg-12">
	              <select class="form-control" name="id_tipe">
	                <option value="">Pilih Kelas Kamar</option>
	                <?php foreach ($tipe as $value2){
	                 ?>
	                    <option value="<?php echo $value2['id_tipe'];?>"><?php echo $value2['tipe_kamar'];?></option>
	                <?php
	                }
	                ?>
	               
	              </select>
	              </div>
	          </div>
	          <button class="btn btn-primary">Find Now</button>
	      	</form>

	  </div>





	</div>

	<div class="col-lg-9 col-sm-8">

	<div class="row">


	     <?php
	      foreach ($cari as $value) { ?>
	     <!-- properties -->
	      <div class="col-lg-4 col-sm-6">

	        
	        <div class="properties">
	          <div class="image-holder"><img src="foto_produk/<?=$value['gambar'];?>" class="img-responsive" alt="properties"/>
	            <?php
	            if ($value['status_kamar']=='tersedia') { ?>
	            <div class="status sold">Avaliable</div>

	            <?php
	            }
	            else { ?>

	            <div class="status new">No Avaliable</div>
	            <?php
	            }
	            ?>
	          </div>
	          <h4><a href="#"><?php echo $value['no_kamar'];?></a></h4>
	          <p class="price">Harga: <?=$value['harga_kamar'];?></p>
	          <div class="listing-detail"><?php echo $value['nama_kamar'];?></div>
	          <div class="listing-detail"><?php echo $value['tipe_kamar'];?></div>
	          <a class="btn btn-primary" href="detail.php?id=<?=$value['id_kamar'];?>">selengkapnya</a>
	        </div>


	      </div>
	      <!-- properties -->

	      <?php
	      }
	      ?>

	    
	      <div class="center">

	</div>

	</div>
	</div>
	</div>
	</div>
	</div>

<?php include 'layout/footer.php'; ?>