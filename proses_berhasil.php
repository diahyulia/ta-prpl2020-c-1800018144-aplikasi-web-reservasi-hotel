<?php 

include 'layout/header.php';

$ambil_id = mysqli_query($koneksi, "SELECT MAX(id_sewa) FROM transaksi");
$data=mysqli_fetch_array($ambil_id); 

$id = $data[0];

$transaksi = mysqli_query($koneksi, "SELECT transaksi.id_sewa, transaksi.tgl_bayar, transaksi.id_pelanggan, transaksi.id_kamar, transaksi.tgl_cekin, transaksi.tgl_cekout, transaksi.extend, transaksi.total_extend, transaksi.status_pembayaran, kamar.id_kamar, kamar.id_tipe, kamar.nama_kamar, kamar.no_kamar, kamar.tipe_kasur, kamar.lokasi, kamar.harga_kamar, pelanggan.id_pelanggan, pelanggan.nama_pelanggan, pelanggan.gender, pelanggan.no_telp, pelanggan.alamat, pelanggan.email FROM transaksi join kamar ON transaksi.id_kamar = kamar.id_kamar join pelanggan on transaksi.id_pelanggan = pelanggan.id_pelanggan WHERE transaksi.id_sewa = '$id'");

foreach ($transaksi as $data):
?>

<div class="container">
	<div class="bg-white" style="height: 500px;">
		<table width="50%">
			<tr>
				<td>id sewa</td>
				<td>:</td>
				<td><?php echo $data['id_sewa'];?></td>
			</tr>
			<tr>
				<td>nama pelanggan</td>
				<td>:</td>
				<td><?php echo $data['nama_pelanggan'];?></td>
			</tr>
			<tr>
				<td>tgl cekin</td>
				<td>:</td>
				<td><?php echo $data['tgl_cekin'];?></td>
			</tr>
			<tr>
				<td>tgl cekout</td>
				<td>:</td>
				<td><?php echo $data['tgl_cekout'];?></td>
			</tr>
			<tr>
				<td>kamar</td>
				<td>:</td>
				<td><?php echo $data['nama_kamar'];?></td>
			</tr>
			<tr>
				<td>no kamar</td>
				<td>:</td>
				<td><?php echo $data['no_kamar'];?></td>
			</tr>
			<tr>
				<td>tipe kasur</td>
				<td>:</td>
				<td><?php echo $data['tipe_kasur'];?></td>
			</tr>
			<tr>
				<td>lokasi</td>
				<td>:</td>
				<td><?php echo $data['lokasi'];?></td>
			</tr>
			<tr>
				<td>harga</td>
				<td>:</td>
				<td><?php echo $data['harga_kamar'];?></td>
			</tr>
			<tr>
				<td>total pembayaran</td>
				<td>:</td>
				<td><?php echo $data['total_extend'];?></td>
			</tr>
		</table>

		<a href="download.php?id=<?=$data['id_sewa'];?>">
			<button class="download">Download</button>
		</a>

	</div>
</div>
<?php 

endforeach;

include 'layout/footer.php'; 

?>